/* Author: 
    Ninad
*/

var 
menuBtn = document.querySelector('.menu_btn'),
nav = document.querySelector('.navigation'),
navItems = document.querySelector('.nav_items'),
menuOpen = false;

//For Menu Toggle
menuBtn.addEventListener('click', () => {
    if(!menuOpen) {
        menuBtn.classList.add('open');
        nav.classList.add('nav_open');
        setTimeout(function() {
            navItems.classList.add('nav_items_display');
        }, 300);
        menuOpen = true;
    } else {
        menuBtn.classList.remove('open');
        nav.classList.remove('nav_open');
        navItems.classList.remove('nav_items_display');
        menuOpen = false;
    }
});